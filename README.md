# asclinic-system

Software for running the robot that is the focus of the Autonomous Systems Clinic subject.

# Introduction
This project aims to develop a fully automatic robot that can automatically patrol an indoor space and 
take pictures of plants in the space. The robot uses Jetson Xavier NX as a microprocessor, ROS as an 
operating system, and works in conjunction with components such as cameras, motors, encoders, and 
aruco markers to complete functions like path planning, positioning, tracking, plant identification, and 
photography. The robot comprises seven subsystems, namely path planning, wheel odometry, wheel 
speed control, CV localization, Localization via fusion, tracking control, and CV plant detection

