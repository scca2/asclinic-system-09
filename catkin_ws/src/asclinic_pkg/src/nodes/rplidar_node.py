#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sensor_msgs.msg import LaserScan

import rospy

# Respond to subscriber receiving a message
def laserScanSubscriberCallback(msg):
    rospy.loginfo("Message receieved with ranges = " + str((msg.ranges[550]) ))

if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("rp_lider")

    # Initialise a subscriber to the RPLidar scan
    rospy.Subscriber("/asc"+"/scan", LaserScan, laserScanSubscriberCallback)

    # Spin as a single-threaded node
    rospy.spin()

